Source: libgtop2
Section: libs
Priority: optional
Maintainer: Debian GNOME Maintainers <pkg-gnome-maintainers@lists.alioth.debian.org>
Uploaders: Emilio Pozuelo Monfort <pochu@debian.org>,
           Jeremy Bícha <jbicha@ubuntu.com>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gir,
               dh-sequence-gnome,
               gettext,
               gtk-doc-tools (>= 1.4),
               libgirepository1.0-dev (>= 0.10.7-1~),
               libglib2.0-dev (>= 2.6.0),
               libxau-dev,
               libxt-dev,
               pkgconf,
               texinfo
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/gnome-team/libgtop
Vcs-Git: https://salsa.debian.org/gnome-team/libgtop.git

Package: libgtop-2.0-11
Architecture: any
Multi-Arch: same
Depends: libgtop2-common (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Description: gtop system monitoring library (shared)
 The gtop library reads information about processes and the state of the
 system. It is used by the GNOME desktop environment.
 .
 This package contains the shared library.

Package: libgtop2-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: gir1.2-gtop-2.0 (= ${binary:Version}),
         libglib2.0-dev,
         libgtop-2.0-11 (= ${binary:Version}),
         ${misc:Depends}
Description: gtop system monitoring library (devel)
 The gtop library reads information about processes and the state of the
 system. It is used by the GNOME desktop environment.
 .
 This package contains the static library and development headers.

Package: libgtop2-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: gtop system monitoring library (common)
 The gtop library reads information about processes and the state of the
 system. It is used by the GNOME desktop environment.
 .
 This package contains the translations.

Package: libgtop2-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends}
Description: gtop system monitoring library (documentation)
 The gtop library reads information about processes and the state of the
 system. It is used by the GNOME desktop environment.
 .
 This package contains the documentation.

Package: gir1.2-gtop-2.0
Architecture: any
Multi-Arch: same
Section: introspection
Depends: ${gir:Depends},
         ${misc:Depends}
Description: gtop system monitoring library (gir bindings)
 The gtop library reads information about processes and the state of the
 system. It is used by the GNOME desktop environment.
 .
 This package can be used by other packages using the GIRepository format to
 generate dynamic bindings.
